var path = require('path');

module.exports = {
  entry: {
    main: './src/index.js',
  },
  devtool: 'source-map',
  mode: 'development',
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist/')
  },
  module: {
    rules: [
      {
        test: /\.svg(\?.*)?$/, 
        use: [
          'url-loader',
          'svg-transform-loader' 
        ]
      },
      {
        test: /\.(scss|sass)$/i,
        use: [
          "style-loader",
          "css-loader",
          "sass-loader"
        ]
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  }
}