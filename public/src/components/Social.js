import React from "react";

import { Link, Divider } from "@material-ui/core";

class Social extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <React.Fragment>
        <Link href="https://instagram.com/jordanlys95" target="_blank">
          Instagram
        </Link>
        <Divider orientation="vertical" flexItem />
        <Link href="https://linkedin.com/in/jordanlys95" target="_blank">
          LinkedIn
        </Link>
        <Divider orientation="vertical" flexItem />
        <Link href="https://gitlab.com/jordanlys95" target="_blank">
          GitLab
        </Link>
      </React.Fragment>
    );
  }
}

export default Social;
