var express = require('express');
var router = express.Router();

const admin = require('firebase-admin');

const db = admin.firestore();


/* POST. */
router.get('/getWork', function(req, res, next) {
    db.collection('works').get()
    .then(snapshot => {
        let works = []
        snapshot.forEach(doc=>{
            works.unshift(doc.data())
        })
        res.send({works})
    })

    

});

module.exports = router;
