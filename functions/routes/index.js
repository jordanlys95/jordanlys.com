var express = require('express');
var router = express.Router();

/* GET */
router.get('/resume', function(req, res, next) {
  // Portfolio page
  res.render('index', { 
    name : 'Jordan Lim Yingsi',
    position: 'Web Developer',
    summary: 'Resourceful, proactive, and highly motivated professional with at least three years of experience in Web Development.',
    objective: 'Seeking an environment of growth and excellence to utilize my skills and knowledge to achieve the goals of the organization.',
    contact: {
      email: 'jordanlys95@gmail.com',
      mobile: '9018 3355',
      website: 'jordanlys.com'
    },
    experience: [
      {
        company: 'DroneXchange',
        position: 'Co-founder, Web Developer',
        short_desc: 'Created flywhere.sg - a platform that promotes safe and compliant drone operations in Singapore with over 4,500 monthly active users.',
        start: 'August 2019',
        end: 'Present'
      },
      {
        company: 'Freelance Work',
        position: 'Full Stack Web Developer',
        short_desc: 'Built and created a complete appointment scheduling portal that simplifies the process of scheduling parent teacher conferences between parents and teachers in Xishan Primary School.',
        start: 'April 2019',
        end: 'August 2019'
      },
      {
        company: 'DREA Asia',
        position: 'Junior Web Developer',
        short_desc: 'Designed and developed web components for drea.com.sg.',
        start: 'August 2016',
        end: 'February 2018'
      },
      {
        company: 'PSA International',
        position: 'Intern',
        short_desc: 'Developed a division collaboration intranet portal using Microsoft Sharepoint.',
        start: 'March 2016',
        end: 'August 2016'
      },
      {
        company: 'Zenix Technology',
        position: 'Technical Assistant',
        short_desc: 'Worked on hardware projects with Resorts World Sentosa, M1, and National Heart Center Singapore.',
        start: 'December 2013',
        end: 'October 2015'
      },
      {
        company: 'Ribar Industries',
        position: 'Network Administrator',
        short_desc: 'Managed network infrastructure and developed a webpage for ribar.sg.',
        start: 'March 2014',
        end: 'August 2015'
      }
    ],
    education: [
      {
        school: 'Ngee Ann Polytechnic',
        qualification: 'Diploma in Information Technology',
        short_desc: 'Specialization in Solutions Architect',
        start: '2014',
        end: '2018'
      },
      {
        school: 'Institute of Technical Education',
        qualification: 'NITEC in Information Technology',
        short_desc: 'Specialization in Cloud Computing',
        start: '2012',
        end: '2013'
      },
      {
        school: 'Bukit Panjang Government High School',
        qualification: 'GCE \'N\' levels',
        start: '2008',
        end: '2011'
      }

    ],
    skillset: [
      {
        title: 'Web Development',
        tag: [
          'HTML',
          'CSS',
          'JavaScript',
          'Node.JS',
          'PHP',
          'SQL',
          'NoSQL',
          'Vue.js',
          'React.js',
          'Express.js'
        ]
      },
      {
        title: 'Software Development',
        tag: [
          'C#',
          'ASP.NET',
          'Java'
        ]
      },
      {
        title: 'Software',
        tag: [
          'Photoshop',
          'Illustrator',
          'Microsoft Office',
          'Sharepoint Foundation',
          'Git'
        ]
      },
      {
        title: 'Cloud',
        tag: [
          'VMware vSphere / vCenter / vCloud Director',
          'Microsoft Azure',
          'Amazon Web Services',
          'Google Cloud / Firebase'
        ]
      }
    ],
    award:[
      {
        name: 'ITE Director\’s List',
        year: '2012 - 2013'
      },
      {
        name: 'CDC & CCC - ITE Scholarship',
        year: '2012 - 2013'
      },
      {
        name: 'EDUSAVE Award for Achievement, Good Leadership and Service (EAGLES)',
        year: '2012'
      }
      
    ],
    certification: [
      {
        title: 'VMWare Certified Associate',
        tag: [
          'Workplace Mobility',
          'Cloud',
          'Data Center Virtualization'
        ]
      },
      {
        title: 'Microsoft Technical Associate',
        tag: [
          'Networking Fundamentals',
          'Windows Server Administration Fundamentals'
        ]
      }
    ],
    language: [
      {
        title: 'English',
        spoken: 4,
        written: 5
        
      },
      {
        title: 'Mandarin',
        spoken: 3,
        written: 2
      }
    ],
    interest: [
      'Software & Web Development',
      'Graphic & Web Design',
      'Photography & Cinematography',
      'Constructing computer systems'
    ],
    references:[
      {
        name: 'Yuet Whey Siah',
        title: 'CEO',
        company: 'GrowthDesk (formerly known as DREA Asia)', 
        address: '54B Club Street', 
        mobile: '9007 8132', 
        email: 'yuetwhey@growthdesk.com',
        relationship: 'Former Employer'
      },
      {
        name: 'Nur Azarina Khamis',
        title: 'Subject Head (ICT)',
        company: 'Xishan Primary School', 
        address: '8 Yishun Street 21', 
        mobile: '9382 2894', 
        email: 'naklearn@gmail.com',
        relationship: 'Stakeholder, Parent Teacher Conference Portal'
      },
    ]
   });
});

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* POST. */
router.post('/', function(req, res, next) {
  res.send('respond with a resource');
});

module.exports = router;
